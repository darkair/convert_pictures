#!/bin/sh
for fname in *.jpg; do
  echo "processing $fname"
  composite -compose atop -dissolve 60% -geometry x75+25+25 -gravity southeast -resize 640x640 ./watermark/watermark.png ./$fname ./resized/$fname
done

for fname in *.JPG; do
  echo "processing $fname"
  composite -compose atop -dissolve 60% -geometry x75+25+25 -gravity southeast -resize 640x640 ./watermark/watermark.png ./$fname ./resized/$fname
done
